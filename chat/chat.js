var csv_pat = /\s*,\s*/;
var config = (function configure() {
  try {
    return {
      max_emotes: Math.max(0, Number(`{max_big_emotes}`)) || Infinity,
      streamers: new Set(
        `{streamer_names}`
          .split(csv_pat)
          .filter((v) => v != null && v.length > 0)
          .map((v) => v.toLowerCase())
      ),
      use_mock: Boolean(`{dev_mock_streamers_selector}`),
      streamer_avatar: `{streamer_avatar}` || null,
      streamer_color: '{streamer_name_color}' || null,
      chatter_avatar: `{chatter_avatar}` || null,
      chatter_color: '{chatter_name_color}' || null,
      avatar_blacklist: new Set(
        `{avatar_blacklist}`
          .split(csv_pat)
          .filter((v) => v != null && v.length > 0)
          .map((v) => v.toLowerCase())
      ),
      blacklist: new Set(
        `{blacklist}`
          .split(csv_pat)
          .filter((v) => v != null && v.length > 0)
          .map((v) => v.toLowerCase())
      ),
      message_number: 0,
    };
  } catch (err) {
    console.error(err);
    return {
      max_emotes: 8,
      streamers: new Set(),
      use_mock: true,
      streamer_avatar: '',
      chatter_avatar: '',
      avatar_blacklist: new Set(),
      blacklist: new Set(),
      message_number: 0,
    };
  }
})();

document.addEventListener('onLoad', function (obj) {
  // obj will be empty for chat widget
  // this will fire only once when the widget loads

  console.log('chat ready');
  // this is absolutely not needed but its here anyway
  console.log(
    'chat configuration:',
    ...Object.entries(config)
      .map(([k, v]) => ['\n' + k + ':', v])
      .flat()
  );
});

document.addEventListener('onEventReceived', function (obj) {
  console.log('event:', obj);
  // obj will contain information about the event
  patchElement(obj?.detail);
});

function getLog() {
  return document.getElementById('log');
}

function patchElement(detail) {
  switch (detail?.command) {
    case 'PRIVMSG':
      break;
    default:
      return;
  }

  if (!detail?.messageId) {
    mockHost(detail);
    return;
  }

  const ele = document.querySelector(
    "#log>div[data-id='" + detail?.messageId + "']"
  );
  if (ele == null) {
    console.warn(
      'could not find chat element for messageId: %s',
      detail?.messageId
    );
    return;
  }

  const from = detail?.from;
  if (isBlocked(from)) {
    getLog().removeChild(ele);
  }

  const is_streamer = detail?.owner || isStreamer(from);
  ele.setAttribute('data-is-streamer', String(is_streamer));
  if (is_streamer) {
    ele.classList.add('streamer');
    appendStreamerAvatar(ele, detail);
    setNameColor(ele, config.streamer_color);
  } else {
    ele.classList.add('chatter');
    console.log('appending chatter avatar...');
    appendChatterAvatar(ele, detail);
    setNameColor(ele, config.chatter_color);
  }

  patchMessageElement(ele);
}

/** @param {HTMLElement} ele */
function patchMessageElement(ele) {
  const msg = ele.querySelector('.message');
  if (msg == null) {
    console.warn('chat element has no message');
    return;
  }

  /** @type {Array<HTMLSpanElement>} */
  const emotes = Array.prototype.slice.call(msg.querySelectorAll('.emote'));
  const text = msg.textContent.trim();
  msg.setAttribute('data-emotes', emotes.length);

  if (text === '' && emotes.length > 0 && emotes.length <= config.max_emotes) {
    msg.classList.add('emote-only');
  }
}

function mockHost(detail) {
  if (!config.use_mock) {
    return;
  }

  switch (detail?.command) {
    case 'PRIVMSG':
      break;
    default:
      return;
  }

  // select the last child that fulfills the mock criteria
  const log = document.getElementById('log');
  const ele = document.querySelector("#log>div[data-id='']:last-child");

  const num = Number(log.getAttribute('data-count') ?? 0) + 1;
  log.setAttribute('data-count', num);
  const is_streamer = num % 4 === 1;

  if (is_streamer) {
    ele.classList.add('streamer');
    appendStreamerAvatar(ele, detail);
    setNameColor(ele, config.streamer_color);
  } else {
    ele.classList.add('chatter');
    appendChatterAvatar(ele, detail);
    setNameColor(ele, config.chatter_color);
  }
}

function isStreamer(name) {
  return config.streamers.has(String(name).toLowerCase());
}

function isAvatarBlocked(name) {
  return config.avatar_blacklist.has(String(name).toLowerCase());
}

function isBlocked(name) {
  return config.blacklist.has(String(name).toLowerCase());
}

function setNameColor(ele, color) {
  if (color == null) {
    return;
  }

  let meta = ele.querySelector('.meta');
  meta.style.color = color;
}

function appendChatterAvatar(ele, detail) {
  if (!config.chatter_avatar || isAvatarBlocked(detail?.from)) {
    return;
  }

  let avatar = document.createElement('span');
  avatar.classList.add('avatar', 'avatar-chat');
  avatar.style.backgroundImage = `url('${config.chatter_avatar}')`;
  ele.appendChild(avatar);
}

function appendStreamerAvatar(ele, detail) {
  if (!config.streamer_avatar || isAvatarBlocked(detail?.from)) {
    return;
  }

  let avatar = document.createElement('span');
  avatar.classList.add('avatar', 'avatar-streamer');
  avatar.style.backgroundImage = `url('${config.streamer_avatar}')`;
  ele.appendChild(avatar);
}
